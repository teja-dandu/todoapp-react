import React, { Component } from 'react';


class AddTaskForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            inputText: "",
        }
    }
    handleChange = (e) => {
        this.setState({
            inputText: e.target.value,
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.onAddTask(this.state.inputText);
        this.setState({
            inputText: "",
        });
    }

    render() { 
        return (
            <div>
                <form onSubmit = {this.handleSubmit}>
                    <input
                        onChange = {this.handleChange}
                        type ="text"
                        name="todo-text"
                        value={this.state.inputText}
                    />
                    <button type="submit">Add Task</button>
                </form>
            </div>
        );
    }
}
 
export default AddTaskForm;