import React from "react";
import { v4 as uuidv4 } from "uuid";

import AddTaskForm from "../components/AddTaskForm";

const FILTERS = [
    {
        label: "Show all",
        value: "all",
    },
    {
        label: "Show completed",
        value: "completed",
    },
    {
        label: "Show active",
        value: "active",
    }
]

// React state to the input component
// If input changes, then state is chaning => render => UI gets the latest value from state
class TodoContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [
            // {
            // text: "The third Todo",
            // completed: false,
            // id:uuidv4(),
            // },
            // {
            //     text: "The Second Todo",
            //     completed: false,
            //     id:uuidv4(),
            // },
            // {
            //     text: "The first Todo ",
            //     completed: false,
            //     id:uuidv4(),
            // },
        ], // proper state value
        activeFilter: "all"
    };
  }

  // Removing a task
  // Checking a task

  handleAddTask = (text) => {
    let newTask = {
      text: text,
      id: uuidv4(),
      completed: false,
    };

    this.setState({
      tasks: [newTask, ...this.state.tasks],
    });
  };

  handleTaskToggle = (id) => {
    this.setState({
      tasks: this.state.tasks.map((task) => {
        if (task.id === id) {
          return {
            ...task,
            completed: !task.completed,
          };
        }
        return task;
      }),
    });
  };

  handleRemoveTask = (id) => {
      this.setState({
          tasks: this.state.tasks.filter(task => task.id !== id),
      });
  }
  getFilteredTask = () => {
      const {activeFilter} = this.state;
      if(activeFilter === 'all'){
          return this.state.tasks;
      }else if(activeFilter === 'completed'){
          return this.state.tasks.filter((t) => t.completed);

      }else if(activeFilter === 'active'){
          return this.state.tasks.filter((t) => !t.completed);
      }
      return [];
  }

  handleSetFilter = (filter) => {
    this.setState({
        activeFilter: filter.value,
    });
  }

  

  render() {
    const activeTasksLength = this.state.tasks.filter(
      (t) => !t.completed
    ).length;

    const tasksToBeDisplayed = this.getFilteredTask();
    return (
      <div style={{
          width: "60%",
          display: "flex",
          flexDirection: "column",
          margin: "0 auto",
          padding: "4rem",
      }}>
        <AddTaskForm onAddTask={this.handleAddTask} />
        <p># of tasks:{activeTasksLength}</p>
        <ul>
          {tasksToBeDisplayed.map(({ text, id, completed }) => {
            return (
              <li
                style={{
                  display: "flex",
                  width: "100%",
                  margin: "1rem",
                }}
                key={id}
              >
                <form>
                    <input
                    type="checkbox"
                    name="task-completed"
                    checked={completed}
                    onChange={() => this.handleTaskToggle(id)}
                    />
                </form>
                
                {text}
                <button style={{marginLeft:"auto"}}
                onClick={() => this.handleRemoveTask(id)}
                >
                X
                </button>
              </li>
            );
          })}
        </ul>
        <ul
            style={{
                display: "flex",
                width: "100%",
                justifyContent: "space-around"
            }}
        >
        {FILTERS.map((filter) => {
            return <li onClick ={() => this.handleSetFilter(filter)}key={filter.label}>{filter.label}</li>
        })}
        </ul>
      </div>
    );
  }
}

export default TodoContainer;